function makeImage(str,G) {
	var longSetting = JSON.parse(G.get("longSetting"));
	var img = new Image();
	if(longSetting.sy){
		img.src = longSetting.sy;
	}
	var len = parseFloat(longSetting.len);
	var i = 0;
	var fontFml = longSetting.fontFamily;
	var fontSize = parseFloat(longSetting.fontSize);
	var padding = parseFloat(longSetting.padding);
	var fontWeight = 'normal';
	var txt = str;
	var canvas = document.createElement("canvas");
	
	if (len > txt.length) {
		len = txt.length;
	}
	var fontHeight = fontSize * (3 / 2)* (Math.ceil(txt.length / len) + txt.split('\n').length - 1) + (padding * 2);
	var fontWidth = fontSize * len + (padding * 2);

    if(img.height != 0){
		var w = img.width,h = img.height;
		if(fontWidth < img.width){
			var bl = fontWidth/img.width;
			img.width = fontWidth;
			img.height = h*bl;
		}
	}	
	canvas.width = fontWidth;
	canvas.height = fontHeight + img.height;
	var context = canvas.getContext('2d');
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = "#fff";
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = longSetting.color;
	context.font = fontWeight + ' ' + fontSize + 'px ' + fontFml + ',arial';
	context.textBaseline = 'top';
	canvas.style.display = 'none';
	function fillTxt(text) {
		while (text.length > len) {
			var txtLine = text.substring(0, len);
			text = text.substring(len);
			context.fillText(txtLine, padding, padding + fontSize * (3 / 2) * i++,
				canvas.width);
		}
		
		context.fillText(text, padding, padding + fontSize * (3 / 2) * i, canvas.width);
	}
	var txtArray = txt.split('\n');
	for (var j = 0; j < txtArray.length; j++) {
		fillTxt(txtArray[j]);
		context.fillText('\n', 0, fontSize * (3 / 2) * i++, canvas.width);
	}
	
	if(img.height != 0){
		var x = 0;
		if(fontWidth - img.width > 0){
			x = (fontWidth - img.width)/2;
		}
		context.drawImage(img, x, fontHeight , img.width, img.height);
	}
	
	var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < canvas.width * canvas.height* 4; i += 4) {
         imageData.data[i + 3] = 128;
    }
	return canvas.toDataURL('image/jpeg',parseFloat(longSetting.imgQuality||1));
}