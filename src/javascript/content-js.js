var sina_event_json = '';
				
function sina_getImages(picMinWidth,picMinHeight,picMaxWidth,picMaxHeight){

		var picArray = new Array();
		var j = 0;
		var imgs = document.getElementsByTagName("img");

		if(imgs.length < 1){
			console.log("无符合条件的图片。");
			return;
		}
			
		for(var i=0 ; i < imgs.length; i++){
			if(imgs[i].offsetWidth > picMinWidth && imgs[i].offsetHeight > picMinHeight
			&& imgs[i].offsetWidth < picMaxWidth && imgs[i].offsetHeight < picMaxHeight){  
				picArray[j] = imgs[i].src;
				j++;		
			}
		}

		sina_event_json = '';
		try{
			sina_event_json = "{\"picList\":[";
			for(i = 0;i < picArray.length;i++)
				sina_event_json +='{"src":"'+ picArray[i] + '"},';
			sina_event_json = sina_event_json.substring(0,sina_event_json.lastIndexOf(',')); //去掉最后一个数据添加到逗号
			sina_event_json = sina_event_json + ']}';						
			localStorage["sina_picList"] = sina_event_json;
		}catch(e){
			console.log("数据格式错误.Err_");		
		}
		
		//完成并输出
		chrome.extension.sendRequest({"sina_event_json": sina_event_json,"hope":"addpic"}, function(response) {});
}

chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
			switch(request.hope)
			{
				case "getImages":
					sina_getImages(request.minWidth,request.minHeight,request.maxWidth,request.maxHeight);
					break;
			}
			sendResponse({}); // Send nothing..
	});