﻿$(function () {
	if(parseInt(G.get('unread_count')) > 0 && (parseInt(G.get('status')) > 0 || parseInt(G.get('follower')) > 0 || parseInt(G.get('cmt')) > 0 || parseInt(G.get('mention_status')) > 0 || parseInt(G.get('mention_cmt')) > 0)){
		window.location.replace('notification_popup.html');
		return;
	}
	
	if(parseInt(G.get('unread_count')) > 0){
		G.clearCounts();
		G.set('unread_count',0);
	}
	
	$("#lagout").click(change_user);
	$("#share").click(share);
	$("#good").click(good);
	$("#ajaximg").click(function(){
		unSelectFile('_list');
	}).change(function(){
		selectFile(this);
	});
	$("#status").keyup(function(){
		checkText('status', 140);
	});
	$("#img_select").hover(function(){
		$('#img_pop').show();
	},function(){
		$('#img_pop').hide();
	});
	$("#unSelectFile").click(function(){unSelectFile();});
	$("#_unSelectFile").click(function(){
		unSelectFile('_list');
	});
	$("#img_list_select").hover(function(){
		$('#img_list_pop').show();
	},function(){
		$('#img_list_pop').hide();
	});
	$("#ajax_listimg").click(unSelectFile).change(function(){
		selectListFile(this);
	});
	
	$("#showFace").click(function(){
		showFace('status', 'facexy', 0);
	});
	$("#insertTopic").click(function(){insertTopic();});
	$("#shortWrap").click(shortWrap);
	$("#at_frends").click(atWrap);
	$("#send_status").click(send_status);
	$("#closewrap_duan").click(function(){
		closewrap('#divWrap');
	});
	$("#closewrap_frend").click(function(){
		closewrap('#atWrap');
	});
	$("#layer_url_btn").click(function(){createShortUrl();});
	
	$('#xversion').text("V"+G.version);
});

function share() {
	chrome.tabs.getSelected(null, function (tab) {
		if (tab.url) {
			$('#status').val(tab.title);
			short_url(tab.url, function (surl) {
				$('#status').val($('#status').val() + " " + surl);
			});
			chrome.tabs.getSelected(null, function(tab) {
			  chrome.tabs.sendRequest(tab.id, {"hope":"getImages",
						"minWidth":10,"minHeight":10,"maxWidth":1000000,"maxHeight":1000000}, function(response) {
				if(G.get("picList")){
					var picList = JSON.parse(G.get("picList"));
					if(picList.picList && picList.picList.length > 0){
						showImg(picList.picList);
					}
				}
			  });
			});
		}
		$('#status').focus();
		checkText("status", 140);
	});
}

function showImg(picList) {
	$("#atWrap").hide();
	$("#divWrap").hide();
	$("#imgWrap").empty();
	
	$.each(picList,function(){
		$("#imgWrap").append("<img src='"+this.src+"'/>");
	});
	$("#imgWrap img").click(function(){
		$("#img_upload").hide();
		$("#img_name").show();
		$("#img_select").text("分享图片");
		$("#img_pop").append($(this).clone());
	});
	
	$("#imgWrap").toggle(); //slideToggle
	
	var offset = $("#facexy").offset();
	$("#imgWrap").css("left", 32).css("bottom", 66);

	$(document.body).click(function (e) {
		$("#imgWrap").hide();
	});
	$(document.body).scroll(function (e) {
		$("#imgWrap").hide();
	});
	
}

function change_user() {
	G.logout();
	window.location = window.location;
}