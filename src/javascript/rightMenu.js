﻿var longSetting = SINA.get("longSetting");
if(longSetting){
longSetting = JSON.parse(longSetting);
}
if(longSetting && longSetting["showRightMenu"]){
/**
 * 右键事件
 *
 */
chrome.contextMenus.create({
	"title" : "发送这句话到新浪微博(&F)",
	"contexts" : ["selection"],
	"onclick" : function (info, tab) {
		chrome.tabs.create({
			"url" : "send.html?text=" + encodeURIComponent(info.selectionText) + "&godsonUrl=" + encodeURIComponent(info.pageUrl),
			"index" : (tab.index + 1)
		});
	}
});

chrome.contextMenus.create({
	"title" : "分享链接地址到新浪微博(&A)",
	"contexts" : ["link"],
	"onclick" : function (info, tab) {
		var text = "好东西大家分享";
		if (info.selectionText) {
			text = info.selectionText;
		}
		chrome.tabs.create({
			"url" : "send.html?text=" + encodeURIComponent(text) + "&godsonUrl=" + encodeURIComponent(info.linkUrl),
			"index" : (tab.index + 1)
		});
	}
});

chrome.contextMenus.create({
	"title" : "分享当前地址到新浪微博(&D)",
	"contexts" : ["page"],
	"onclick" : function (info, tab) {
		var title = tab.title;
		chrome.tabs.create({
			"url" : "send.html?text=" + (encodeURIComponent(title) || "") + "&godsonUrl=" + encodeURIComponent(info.pageUrl),
			"index" : (tab.index + 1)
		});
	}
});

chrome.contextMenus.create({
	"title" : "关注开发者,反馈意见",
	"contexts" : ["page"]
});

chrome.contextMenus.create({
	"title" : "分享这张图片到新浪微博(&E) ",
	"contexts" : ["image"],
	"onclick" : function (info, tab) {
		var url = 'http://service.weibo.com/share/share.php?title=SELTEXT&appkey=41484100';
		var posturl = url + '&pic=' + encodeURIComponent(info.srcUrl);
		chrome.tabs.create({
			"url" : posturl.replace("SELTEXT", encodeURIComponent(tab.title)),
			"index" : (tab.index + 1)
		});
	}
});
}