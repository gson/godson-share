$(function () {
	if (!G.sinaAdpt.getAccessToken()) {
		alert("请先登陆！");
		window.close();
	}
	
	var param = G.getQueryString(window.location.href);
	if (!param)
		return;
	
	if (param['ctext']) {
		$("#status").val(decodeURIComponent(param['ctext']));
		checkText("status", 140);
	}
	if (param['text']) {
		$("#status").val(decodeURIComponent(param['text']));
		checkText("status", 140);
	}
	if (param['godsonUrl']) {
		var url = param['godsonUrl'];
		short_url(decodeURIComponent(url), function (sturl) {
			var cvalue = $("#status").val();
			$("#status").val(cvalue + " " + sturl);
			checkText("status", 140);
		});
	}
	
	$("#status").keyup(function(){
		checkText('status', 140);
	});
	
	$("#ajaximg").change(function(){
		selectFile(this);
	});
	
	$("#ajax_listimg").change(function(){
		selectListFile(this);
	});
	
	$("#unSelect").click(function(){unSelectFile()});
	
	$("#unSelectm").click(function(){
		unSelectFile('_list');
	});
	
	$("#img_list_select").hover(function(){
		$('#img_list_pop').show();
	},function(){
		$('#img_list_pop').hide();
	});
	
	$("#img_select").hover(function(){
		$('#img_pop').show();
	},function(){
		$('#img_pop').hide();
	});
	
	$("#showFace").click(function(){
		showFace('status', 'facexy', 0);
	});
	
	$("#insertTopic").click(function(){
		insertTopic();
	});
	
	$("#shortWrap").click(function(){
		shortWrap();
	});
	
	$("#at_frends").click(function(){
		atWrap();
	});
	$("#send_status").click(function(){
		send_status();
	});
	$("#closeDivWrap").click(function(){
		closewrap('#divWrap')
	});
	$("#closeAtWrap").click(function(){
		closewrap('#atWrap')
	});
	$("#layer_url_btn").click(function(){
		createShortUrl()
	});
	
	$('#xversion').text("V"+G.version);
});
