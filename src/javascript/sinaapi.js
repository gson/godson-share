APIV = {
	//微博
	statuses : {
		//读取接口
		"public_timeline" : "https://api.weibo.com/2/statuses/public_timeline.json", //获取最新的公共微博
		"friends_timeline" : "https://api.weibo.com/2/statuses/friends_timeline.json", //获取当前登录用户及其所关注用户的最新微博
		"home_timeline" : "https://api.weibo.com/2/statuses/home_timeline.json", //获取当前登录用户及其所关注用户的最新微博
		"friends_timeline_ids" : "https://api.weibo.com/2/statuses/friends_timeline/ids.json", //获取当前登录用户及其所关注用户的最新微博的ID
		"user_timeline" : "https://api.weibo.com/2/statuses/user_timeline.json", //获取用户发布的微博
		"user_timeline_ids" : "https://api.weibo.com/2/statuses/user_timeline/ids.json", //获取用户发布的微博的ID
		"repost_timeline" : "https://api.weibo.com/2/statuses/repost_timeline.json", //返回一条原创微博的最新转发微博
		"repost_timeline_ids" : "https://api.weibo.com/2/statuses/repost_timeline/ids.json", //获取一条原创微博的最新转发微博的ID
		"repost_by_me" : "https://api.weibo.com/2/statuses/repost_by_me.json", //返回用户转发的最新微博
		"mentions" : "https://api.weibo.com/2/statuses/mentions.json", //获取@当前用户的最新微博
		"mentions_ids" : "https://api.weibo.com/2/statuses/mentions/ids.json", //获取@当前用户的最新微博的ID
		"bilateral_timeline" : "https://api.weibo.com/2/statuses/bilateral_timeline.json", //获取双向关注用户的最新微博
		"show" : "https://api.weibo.com/2/statuses/show.json", //根据ID获取单条微博信息
		"querymid" : "https://api.weibo.com/2/statuses/querymid.json", //通过id获取mid
		"queryid" : "https://api.weibo.com/2/statuses/queryid.json", //通过mid获取id
		"hot_repost_daily" : "https://api.weibo.com/2/statuses/hot/repost_daily.json", //按天返回热门转发榜
		"hot_repost_weekly" : "https://api.weibo.com/2/statuses/hot/repost_weekly.json", //按周返回热门转发榜
		"hot_comments_daily" : "https://api.weibo.com/2/statuses/hot/comments_daily.json", //按天返回热门评论榜
		"hot_comments_weekly" : "https://api.weibo.com/2/statuses/hot/comments_weekly.json", //按周返回热门评论榜
		"count" : "https://api.weibo.com/2/statuses/count.json", //批量获取指定微博的转发数评论数
		//写入接口
		"repost" : "https://api.weibo.com/2/statuses/repost.json", //转发一条微博信息
		"destroy" : "https://api.weibo.com/2/statuses/destroy.json", //删除微博信息
		"update" : "https://api.weibo.com/2/statuses/update.json", //发布一条微博信息
		"upload" : "https://api.weibo.com/2/statuses/upload.json", //上传图片并发布一条微博
		"upload_url_text" : "https://api.weibo.com/2/statuses/upload_url_text.json" //发布一条微博同时指定上传的图片或图片url <高级>
	},
	//评论
	comments : {
		//读取接口
		"show" : "https://api.weibo.com/2/comments/show.json", //获取某条微博的评论列表
		"by_me" : "https://api.weibo.com/2/comments/by_me.json", //我发出的评论列表
		"to_me" : "https://api.weibo.com/2/comments/to_me.json", //我收到的评论列表
		"timeline" : "https://api.weibo.com/2/comments/timeline.json", //获取用户发送及收到的评论列表
		"mentions" : "https://api.weibo.com/2/comments/mentions.json", //获取@到我的评论
		"show_batch" : "https://api.weibo.com/2/comments/show_batch.json", //批量获取评论内容
		//写入接口
		"create" : "https://api.weibo.com/2/comments/create.json", //评论一条微博
		"destroy" : "https://api.weibo.com/2/comments/destroy.json", //删除一条评论
		"destroy_batch" : "https://api.weibo.com/2/comments/destroy_batch.json", //批量删除评论
		"reply" : "https://api.weibo.com/2/comments/reply.json" //回复一条微博
	},
	//关系
	friendships : {
		//关注读取接口
		"friends" : "https://api.weibo.com/2/friendships/friends.json", //获取用户的关注列表
		"friends_in_common" : "https://api.weibo.com/2/friendships/friends/in_common.json", //获取共同关注人列表
		"friends_bilateral" : "https://api.weibo.com/2/friendships/friends/bilateral.json", //获取双向关注列表
		"friends_bilateral_ids" : "https://api.weibo.com/2/friendships/friends/bilateral/ids.json", //获取双向关注UID列表
		"friends_ids" : "https://api.weibo.com/2/friendships/friends/ids.json", //获取用户关注对象UID列表
		//粉丝读取接口
		"followers" : "https://api.weibo.com/2/friendships/followers.json", //获取用户粉丝列表
		"followers_ids" : "https://api.weibo.com/2/friendships/followers/ids.json", //获取用户粉丝UID列表
		"followers_active" : "https://api.weibo.com/2/friendships/followers/active.json", //获取用户优质粉丝列表
		//关系链读取接口
		"friends_chain_followers" : "https://api.weibo.com/2/friendships/friends_chain/followers.json", //获取我的关注人中关注了指定用户的人
		//关系读取接口
		"show" : "https://api.weibo.com/2/friendships/show.json", //获取两个用户关系的详细情况
		//写入接口
		"create" : "https://api.weibo.com/2/friendships/create.json", //关注某用户
		"destroy" : "https://api.weibo.com/2/friendships/destroy.json", //取消关注某用户
		"update" : "https://api.weibo.com/2/friendships/remark/update.json" //更新关注人备注
	},
	//账号
	account : {
		//读取接口
		"get_privacy" : "https://api.weibo.com/2/account/get_privacy.json", //获取隐私设置信息
		"school_list" : "https://api.weibo.com/2/account/profile/school_list.json", //获取所有学校列表
		"rate_limit_status" : "https://api.weibo.com/2/account/rate_limit_status.json", //获取当前用户API访问频率限制
		"get_uid" : "https://api.weibo.com/2/account/get_uid.json", //OAuth授权之后获取用户UID（作用相当于旧版接口的 account/verify_credentials）
		//写入接口
		"end_session" : "https://api.weibo.com/2/account/end_session.json" //退出登录
	},
	users:{
		"show":"https://api.weibo.com/2/users/show.json", //获取用户信息
		"domain_show":"https://api.weibo.com/2/users/domain_show.json", //通过个性域名获取用户信息
		"counts":"https://api.weibo.com/2/users/counts.json" //批量获取用户的粉丝数、关注数、微博数
	},
	//收藏
	favorites : {
		//读取接口
		"favorites" : "https://api.weibo.com/2/favorites.json", //获取当前用户的收藏列表
		"ids" : "https://api.weibo.com/2/favorites/ids.json", //获取当前用户的收藏列表的ID
		"show" : "https://api.weibo.com/2/favorites/show.json", //获取单条收藏信息
		"by_tags" : "https://api.weibo.com/2/favorites/by_tags.json", //获取当前用户某个标签下的收藏列表
		"tags" : "https://api.weibo.com/2/favorites/tags.json", //当前登录用户的收藏标签列表
		"by_tags_ids" : "https://api.weibo.com/2/favorites/by_tags/ids.json", //获取当前用户某个标签下的收藏列表的ID
		//写入接口
		"create" : "https://api.weibo.com/2/favorites/create.json", //添加收藏
		"destroy" : "https://api.weibo.com/2/favorites/destroy.json", //删除收藏
		"destroy_batch" : "https://api.weibo.com/2/favorites/destroy_batch.json", //批量删除收藏
		"tags_update" : "https://api.weibo.com/2/favorites/tags/update.json", //更新收藏标签
		"tags_update_batch" : "https://api.weibo.com/2/favorites/tags/update_batch.json", //更新当前用户所有收藏下的指定标签
		"tags_destroy_batch" : "https://api.weibo.com/2/favorites/tags/destroy_batch.json" //删除当前用户所有收藏下的指定标签
	},
	//话题
	trends : {
		//读取接口
		"trends" : "https://api.weibo.com/2/trends.json", //获取某人话题
		"is_follow" : "https://api.weibo.com/2/trends/is_follow.json", //是否关注某话题
		"hourly" : "https://api.weibo.com/2/trends/hourly.json", //返回最近一小时内的热门话题
		"daily" : "https://api.weibo.com/2/trends/daily.json", //返回最近一天内的热门话题
		"weekly" : "https://api.weibo.com/2/trends/weekly.json", //返回最近一周内的热门话题
		//写入接口
		"follow" : "https://api.weibo.com/2/trends/follow.json", //关注某话题
		"destroy" : "https://api.weibo.com/2/trends/destroy.json" //取消关注的某一个话题
	},
	//标签
	tags : {
		//读取接口
		"tags" : "https://api.weibo.com/2/tags.json", //返回指定用户的标签列表
		"tags_batch" : "https://api.weibo.com/2/tags/tags_batch.json", //批量获取用户标签
		"suggestions" : "https://api.weibo.com/2/tags/suggestions.json", //返回系统推荐的标签列表
		//写入接口
		"create" : "https://api.weibo.com/2/tags/create.json", //添加用户标签
		"destroy" : "https://api.weibo.com/2/tags/destroy.json", //删除用户标签
		"destroy_batch" : "https://api.weibo.com/2/tags/destroy_batch.json" //批量删除用户标签
	},
	//注册
	register : {
		"verify_nickname" : "https://api.weibo.com/2/register/verify_nickname.json" //验证昵称是否可用
	},
	//搜索
	search : {
		//搜索联想接口
		"users" : "https://api.weibo.com/2/search/suggestions/users.json", //搜用户搜索建议
		"statuses" : "https://api.weibo.com/2/search/suggestions/statuses.json", //搜微博搜索建议
		"schools" : "https://api.weibo.com/2/search/suggestions/schools.json", //搜学校搜索建议
		"companies" : "https://api.weibo.com/2/search/suggestions/companies.json", //搜公司搜索建议
		"apps" : "https://api.weibo.com/2/search/suggestions/apps.json", //搜应用搜索建议
		"at_users" : "https://api.weibo.com/2/search/suggestions/at_users.json", //@联想搜索
		//搜索话题接口
		"topics" : "https://api.weibo.com/2/search/topics.json" //搜索某一话题下的微博
	},
	//推荐
	suggestions : {
		//读取接口
		"users_hot" : "https://api.weibo.com/2/suggestions/users/hot.json", //获取系统推荐用户
		"may_interested" : "https://api.weibo.com/2/suggestions/users/may_interested.json", //获取用户可能感兴趣的人
		"by_status" : "https://api.weibo.com/2/suggestions/users/by_status.json", //根据微博内容推荐用户
		"statuses_hot" : "https://api.weibo.com/2/suggestions/statuses/hot.json", //获取微博精选推荐
		"statuses_reorder" : "https://api.weibo.com/2/suggestions/statuses/reorder.json", //主Feed微博按兴趣推荐排序
		"reorder_ids" : "https://api.weibo.com/2/suggestions/statuses/reorder/ids.json", //主Feed微博按兴趣推荐排序的微博ID
		"favorites_hot" : "https://api.weibo.com/2/suggestions/favorites/hot.json", //热门收藏
		//写入接口
		"not_interested" : "https://api.weibo.com/2/suggestions/users/not_interested.json" //不感兴趣的人
	},
	//提醒
	remind : {
		//读取接口
		"unread_count" : "http://rm.api.weibo.com/2/remind/unread_count.json", //获取某个用户的各种消息未读数
		//写入接口
		"set_count" : "https://rm.api.weibo.com/2/remind/set_count.json", //对当前登录用户某一种消息未读数进行清零,
		"unread_count2" : "https://rm.api.weibo.com/2/remind/unread_count.json"
	},
	//短链
	short_url : {
		//转换接口
		"shorten" : "https://api.weibo.com/2/short_url/shorten.json", //长链转短链
		"expand" : "https://api.weibo.com/2/short_url/expand.json", //短链转长链
		//数据接口
		"clicks" : "https://api.weibo.com/2/short_url/clicks.json", //获取短链接的总点击数
		"referers" : "https://api.weibo.com/2/short_url/referers.json", //获取一个短链接点击的referer来源和数量
		"locations" : "https://api.weibo.com/2/short_url/locations.json", //获取一个短链接点击的地区来源和数量
		"counts" : "https://api.weibo.com/2/short_url/share/counts.json", //获取短链接在微博上的微博分享数
		"statuses" : "https://api.weibo.com/2/short_url/share/statuses.json", //获取包含指定单个短链接的最新微博内容
		"comment_counts" : "https://api.weibo.com/2/short_url/comment/counts.json", //获取短链接在微博上的微博评论数
		"comments" : "https://api.weibo.com/2/short_url/comment/comments.json", //获取包含指定单个短链接的最新微博评论
		"info" : "https://api.weibo.com/2/short_url/info.json" //批量获取短链接的富内容信息
	},
	//公共服务
	common : {
		//读取接口
		"code_to_location" : "https://api.weibo.com/2/common/code_to_location.json", //通过地址编码获取地址名称
		"get_city" : "https://api.weibo.com/2/common/get_city.json", //获取城市列表
		"get_province" : "https://api.weibo.com/2/common/get_province.json", //获取省份列表
		"get_country" : "https://api.weibo.com/2/common/get_country.json", //获取国家列表
		"get_timezone" : "https://api.weibo.com/2/common/get_timezone.json" //获取时区配置表
	},
	//地理信息
	location : {
		//基础位置读取接口
		"get_map_image" : "https://api.weibo.com/2/location/base/get_map_image.json", //生成一张静态的地图图片
		//坐标转换接口
		"ip_to_geo" : "https://api.weibo.com/2/location/geo/ip_to_geo.json", //根据IP地址返回地理信息坐标
		"address_to_geo" : "https://api.weibo.com/2/location/geo/address_to_geo.json", //根据实际地址返回地理信息坐标
		"geo_to_address" : "https://api.weibo.com/2/location/geo/geo_to_address.json", //根据地理信息坐标返回实际地址
		"gps_to_offset" : "https://api.weibo.com/2/location/geo/gps_to_offset.json", //根据GPS坐标获取偏移后的坐标
		"is_domestic" : "https://api.weibo.com/2/location/geo/is_domestic.json", //判断地理信息坐标是否是国内坐标
		//POI数据读取接口
		"show_batch" : "https://api.weibo.com/2/location/pois/show_batch.json", //批量获取POI点的信息
		"by_location" : "https://api.weibo.com/2/location/pois/search/by_location.json", //根据关键词按地址位置获取POI点的信息
		"by_geo" : "https://api.weibo.com/2/location/pois/search/by_geo.json", //根据关键词按坐标点范围获取POI点的信息
		"by_area" : "https://api.weibo.com/2/location/pois/search/by_area.json", //根据关键词按矩形区域获取POI点的信息
		//POI数据写入接口
		"add" : "https://api.weibo.com/2/location/pois/add.json", //提交一个新增的POI点信息
		//移动服务读取接口
		"get_location" : "https://api.weibo.com/2/location/mobile/get_location.json", //根据移动基站WIFI等数据获取当前位置信息
		//交通路线读取接口
		"drive_route" : "https://api.weibo.com/2/location/line/drive_route.json", //根据起点与终点数据查询自驾车路线信息
		"bus_route" : "https://api.weibo.com/2/location/line/bus_route.json", //根据起点与终点数据查询公交乘坐路线信息
		"bus_line" : "https://api.weibo.com/2/location/line/bus_line.json", //根据关键词查询公交线路信息
		"bus_station" : "https://api.weibo.com/2/location/line/bus_station.json", //根据关键词查询公交站点信息
		//地理位置信息接口错误代码及解释
		"error2" : "https://api.weibo.com/2/location/error2" // 地理位置信息接口错误代码及解释
	},
	//OAuth 2.0接口（V2版接口将仅支持OAuth 2.0授权方式）
	oauth : {
		"access_token" : "https://api.weibo.com/oauth2/access_token", //获取授权过的Access Token
		"authorize" : "https://api.weibo.com/oauth2/authorize", //请求用户授权Token
		"get_oauth2_token" : "https://api.weibo.com/oauth2/get_oauth2_token" //OAuth1.0的Access Token更换至OAuth2.0的Access Token
	},
	other : {
		"emotions" : "https://api.weibo.com/2/emotions.json" //获取官方表情
	},
	//私信接口
	direct:{
		"direct_messages":"https://api.weibo.com/2/direct_messages.json", //获取当前登录用户收到的最新私信列表
		"show_batch":"https://api.weibo.com/2/direct_messages/show_batch.json",//根据私信ID批量获取私信内容
		"destroy_batch":"https://api.weibo.com/2/direct_messages/destroy_batch.json",//根据私信ID或用户UID批量删除私信
		"new":"https://api.weibo.com/2/direct_messages/new.json",//发送一条私信
		"user_list":"https://api.weibo.com/2/direct_messages/user_list.json",//获取与当前登录用户有私信往来的用户列表，与该用户往来的最新私信
		"is_capable":"https://api.weibo.com/2/direct_messages/is_capable.json"//判断是否可以给对方发私信
	}
};
