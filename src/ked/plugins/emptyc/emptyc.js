/*******************************************************************************
* KindEditor - WYSIWYG HTML Editor for Internet
* Copyright (C) 2006-2011 kindsoft.net
*
* @author Roddy <luolonghao@gmail.com>
* @site http://www.kindsoft.net/
* @licence http://www.kindsoft.net/license.php
*******************************************************************************/

KindEditor.plugin('emptyc', function(K) {
	var self = this, name = 'emptyc', undefined;
	self.clickToolbar(name, function(){
		if(confirm("确定要清空当前内容吗?")){
			self.html("");
			var cmd = self.cmd;
			var body = K(cmd.doc.body);
			body.removeAttr('style');
			localStorage.removeItem('sina_ked_bodyCss');
			localStorage.removeItem('sina_ked_backgroundImage');
			localStorage.removeItem('sina_ked_fullHtml');
		}
	});
});
