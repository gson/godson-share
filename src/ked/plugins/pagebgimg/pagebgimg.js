KindEditor.plugin('pagebgimg', function (K) {
	var self = this,
	name = 'pagebgimg';
	self.clickToolbar(name, function () {
		var Dialog = self.createDialog({
				name : "pagebgimg",
				width : 300,
				height : 140,
				title : "选择背景图片",
				body : '<div style="padding: 10px;"><div><input type="file" id="ke-pagebgimg" accept="image/*" name="imgFile" style=" padding: 0; margin: 0; "></div><div style=" margin-top: 10px; ">图片位置：<input name="pos" type="radio" checked="checked" value="0">正常 <input name="pos" type="radio" value="1">平铺 <input name="pos" type="radio" value="2">拉伸 <input name="pos" type="radio" value="3">居中</div></div>',
				yesBtn : {
					name : self.lang('yes'),
					click : function (e) {
						var imgFiles = document.getElementById('ke-pagebgimg').files;
						var pos = document.getElementsByName('pos');
						var posVlaue = '0';
						for (var i = 0; i < pos.length; i++) {
							if (pos[i].checked) {
								posVlaue = pos[i].value;
								break;
							}
						}
						var cmd = self.cmd;
						var body = K(cmd.doc.body);
						body.css("background-repeat", "");
						body.css("background-size", "");
						body.css("background-position", "");
						if (posVlaue == '0') {
							body.css("background-repeat", "no-repeat");
						} else if (posVlaue == '2') {
							body.css("background-size", "cover");
						} else if (posVlaue == '3') {
							body.css("background-position", "center");
						}
						if (imgFiles.length > 0) {
							var reader = new FileReader();
							reader.readAsDataURL(imgFiles[0]);
							reader.onload = function (e) {
								body.css("background-image", "url(" + e.target.result + ")");
								self.hideDialog();
							};
						} else {
							self.hideDialog();
						}
					}
				}
			});
	});
});