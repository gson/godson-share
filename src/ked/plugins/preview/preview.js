/*******************************************************************************
* KindEditor - WYSIWYG HTML Editor for Internet
* Copyright (C) 2006-2011 kindsoft.net
*
* @author Roddy <luolonghao@gmail.com>
* @site http://www.kindsoft.net/
* @licence http://www.kindsoft.net/license.php
*******************************************************************************/

KindEditor.plugin('preview', function(K) {
	var self = this, name = 'preview', undefined;
	self.clickToolbar(name, function() {
		var bg = chrome.extension.getBackgroundPage();
		var cmd = self.cmd;
		var body = K(cmd.doc.body);
		if(!self.isEmpty()){
			bg['fullHtml'] = self.fullHtml();
			localStorage.setItem('sina_ked_bodyCss',JSON.stringify(body.css()));
			localStorage.setItem('sina_ked_backgroundImage',body.css('background-image'));
			window.open (chrome.extension.getURL('/ked/preview.html'), "preview", "left=400,top=50,height=620, width=800");
		}else{
			alert("要写点什么再看麽？");
		}
	});
});
