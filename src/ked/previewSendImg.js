$(function () {
	var bg = chrome.extension.getBackgroundPage();
	G = bg.SINA;
	var image = new Image();
	image.onload = function(){
		$('body').prepend(image);
	}
	image.src = bg['previewImg'];

	$("#sendBtn").click(function(){
		var zy = window.prompt("请输入摘要：");
		if(typeof zy == 'string'){
			$(".toolbar").hide();
			send_status(zy,bg['previewImg']);
		}
	});
	
	$("#back").click(function(){
		window.location.href = chrome.extension.getURL('/ked/preview.html');
	});
	
	function send_status(weibo,DataURL) {
		var weibo = weibo || "长微博分享";
	
		function reset() {
			G.send = false;
			G.img = null;
			G.file = null;
			window.close();
		}
		function ErrorReset() {
			$(".toolbar").show();
			G.showMsgNotification("发送出错,请重试！");
		}
		G.upload(weibo,dataURLtoBlob(DataURL),reset,ErrorReset);
		G.showMsgNotification("正在后台发送，请不要关闭浏览器！");
		setTimeout(function(){
			window.close();
		},500);
	}
	setTimeout(function(){
		if(document.documentElement.clientWidth <  document.body.scrollWidth){
			window.resizeTo(document.body.scrollWidth + 50);
		}
	},100);
});
